###############################################################################
# (c) Copyright 2019 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = 'Ben Couturier <ben.couturier@cern.ch>'

from LbSoftConfDb2Clients.SoftConfDB import SoftConfDB
from collections import defaultdict
import json
import logging
import os
import timeit


class JSONSoftConfDBCache(object):

    @classmethod
    def create(cls, softconfdb, filename="SoftCondDBCache.json"):
        db = defaultdict(dict)
        projects = softconfdb.listProjects()
        for project in projects:
            logging.info("Processing %s" % project)
            versions = softconfdb.listVersions(project)
            vp = dict()
            for version in versions:
                p, v = version
                platforms = softconfdb.listPlatforms(p, v)
                vp[v] = platforms
            db[project] = vp
        with open(filename, "wt") as f:
            json.dump(db, f)

    def __init__(self, filename="SoftCondDBCache.json"):
        with open(filename, "rt") as f:
            self.db = json.load(f)

    def listPlatforms(self, project, version):
        vp = self.db[project.upper()]
        return vp[version]


def main():
    logging.basicConfig(level=logging.INFO)
    sdb = SoftConfDB("https://lbsoftdb.cern.ch/read/", noCertVerif=True)
    filename = "SoftCondDBCache.json"
    if not os.path.exists(filename):
        JSONSoftConfDBCache.create(softconfdb=sdb)
    else:

        def testjson():
            cache = JSONSoftConfDBCache(filename)
            return cache.listPlatforms("DaVinci", "v50r4")

        logging.info(timeit.timeit(testjson, number=1000))


if __name__ == "__main__":
    main()
