###############################################################################
# (c) Copyright 2019 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__ = 'Ben Couturier <ben.couturier@cern.ch>'

from LbSoftConfDb2Clients.SoftConfDB import SoftConfDB
import logging
import os
import sqlite3
import timeit


def _create_tables(conn):
    """ Create the base structure of the database"""

    create_pv = """    
CREATE TABLE IF NOT EXISTS projects (
 id integer PRIMARY KEY,
 project text NOT NULL,
 version text NOT NULL,
 UNIQUE (project, version)
); 
"""
    conn.execute(create_pv)

    create_platforms = """    
CREATE TABLE IF NOT EXISTS platforms (
 id integer PRIMARY KEY,
 platform text NOT NULL,
 UNIQUE (platform)
);"""
    conn.execute(create_platforms)

    create_project_platforms = """
CREATE TABLE IF NOT EXISTS project_platforms (
project_id integer,
platform_id integer,
FOREIGN KEY (project_id) REFERENCES projects(id),
FOREIGN KEY (platform_id) REFERENCES platforms(id)
); 
"""
    conn.execute(create_project_platforms)
    conn.close()


def _add_platforms(conn, project, version, platforms):
    """ Add platforms for a given project version """

    cursor = conn.cursor()
    cursor.execute("INSERT INTO projects (project, version) VALUES (?, ?)", (project, version))
    id = cursor.lastrowid
    print(id)
    for p in platforms:
        cursor = conn.cursor()
        for row in cursor.execute('SELECT id FROM platforms WHERE platform=?', (p,)):
            (p_id,) = row
            break
        else:
            cursor.execute("INSERT INTO platforms(platform) VALUES(?)", (p,))
            p_id = cursor.lastrowid

        cursor.execute("INSERT INTO project_platforms (project_id, platform_id) VALUES (?, ?)", (id, p_id))


class SoftConfDBCache(object):

    @classmethod
    def create(cls, softconfdb, filename="SoftCondDBCache.sqlite3"):
        cls.create_db(filename)
        cls.fill_db(filename, softconfdb)

    @classmethod
    def create_db(cls, filename):
        # if os.path.exists(filename):
        #    raise Exception("File %s already exists" % filename)

        conn = sqlite3.connect(filename)
        _create_tables(conn)

    @classmethod
    def fill_db(cls, filename, softconfdb):
        conn = sqlite3.connect(filename)
        projects = softconfdb.listProjects()
        for project in projects:
            logging.info("Processing %s" % project)
            versions = softconfdb.listVersions(project)
            for version in versions:
                p, v = version
                platforms = softconfdb.listPlatforms(p, v)
                _add_platforms(conn, p, v, platforms)
        conn.commit()
        conn.close()

    def __init__(self, filename="SoftCondDBCache.sqlite3"):
        self.conn = sqlite3.connect(filename)

    def listPlatforms(self, project, version):
        qry = """
        select platforms.platform from platforms, project_platforms, projects
        where projects.id = project_platforms.project_id
        and project_platforms.platform_id = platforms.id
        and projects.project = ?
        and projects.version = ?
        
        """
        cursor = self.conn.cursor()
        vals = []
        for row in cursor.execute(qry, (project.upper(), version)):
            (val,) = row
            vals.append(val)
        return vals


def main():
    logging.basicConfig(level=logging.INFO)
    sdb = SoftConfDB("https://lbsoftdb.cern.ch/read/", noCertVerif=True)
    filename = "SoftCondDBCache.sqlite3"
    if not os.path.exists(filename):
        SoftConfDBCache.create(softconfdb=sdb)
    else:

        def test():
            cache = SoftConfDBCache(filename)
            return cache.listPlatforms("DaVinci", "v50r4")

        logging.info((timeit.timeit(test, number=1000)))


if __name__ == "__main__":
    main()
